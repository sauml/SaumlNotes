+++
title = "z_draft_Hugo_1_Index_et_presentation"
draft = true
slug = ""
tags = ["draft"]
toc = true
type = "post"
date = "2017-02-22T21:55:43+01:00"

+++

# Introduction
Cet article est le premier d'une série consacrée la mise en place d'un blog à l'aide de Hugo.  
J'y écrierai les différentes étapes que je vais suivre pour publier `Sauml Blog Notes`.  
Je mettrai à jour (amélioration et/ou corrections) les différentes parties en fonction de mon avancée dans la découvertes de Hugo.

- Hugo Partie 1: Index et Présentation.
- Hugo Partie 2: Installer.
- Hugo Partie 3: Configurer.
- Hugo Partie 4: Modifier le thème.
- Hugo Partie 5: Gérer les liens.
- Hugo Partie 6: Déployer.

# Hugo ? C'est Quoi ?
# Et pourquoi avoir choisi Hugo ?
# Lien intéressant
# A suivre...
