+++
toc = true
type = "post"
draft = true
date = "2017-02-22T15:26:01+01:00"
title = "_idPlansAticles"
slug = ""
tags = ["todo"]

+++

- [ ] Un blog pour gérer mes notes.
    - [X] Faire un premier jet / plan
    - [X] Mettre en page
    - [ ] Faire une recherche sur les TODO
    - [ ] Corriger Orthographe et grammaire
- [ ] Hugo
	- S'inspirer de ce [site](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-hugo-a-static-site-generator-on-ubuntu-14-04)
	- et [celui ci](http://rcoedo.com/post/hugo-static-site-generator/)
	- [ ] Index & Présentation
		- [X] ~~Introduire l'article~~
			- [X] ~~Mettre la liste des articles~~
		- [ ] Présenter hugo
		- [ ] Pouquoi l'avoir choisi ?
		- [ ] Liens intéressants
		- [ ] Article suivant
	- Installer
		- !! voir ce qui est déjà fait dans 2017-02-21_passage_blog_sous_hugo
		- Généralités
			- lien vers la page de téléchargement
		- Sous S2
		- Sous W1
		- Créer site
		- Expliquer rapidement structure des répertoires.
		- Premier post
		- Visualiser le résultat
		- Suivant 
	- Configurer
	- Modifier le thème
	- Gérer des liens
	- Déployer
- Pluma
    - Ajouter coloration markdown
        - http://citadel.tistory.com/372
        - les fichiers sont à coller dans /home/samuel/.local/share/pluma/plugins
            - créer les répertoires s'ils n'existent pas
        - Utilisation    
            - Alt-Z on selected lines will collapse them
            - Alt-Z on an indented block's top line will collapse that block
            - Alt-Z on a folded block will expand it
            - Alt-X will collapse all blocks on the deepest indention column (you can keep pressing Alt-X until all indention levels are folded)
            - Shift-Alt-X will expand all the collapsed blocks
- Ajouter taximonie
    - Catégorie
        - rechercher sur le site de hugo comment faire pour avoir des accents
- Mette TableOfContents en gras
